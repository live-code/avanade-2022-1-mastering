export interface Geo {
  lat: string;
  lng: string;
}

export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: Geo;
}

export interface Company {
  name: string;
  catchPhrase: string;
  bs: string;
}

export type Gender = 'M' | 'F';

export interface User {
  id: number;
  name: string;
  gender: Gender;
  username: string;
  email: string;
  address: Address;
  phone: string;
  website: string;
  company: Company;
}


