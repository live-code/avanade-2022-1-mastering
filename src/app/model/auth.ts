
export interface Auth {
  token: string,
  displayName: string;
  role: 'admin' | 'moderator';
}
