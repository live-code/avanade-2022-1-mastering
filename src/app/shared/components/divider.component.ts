import { Component, Input, OnInit } from '@angular/core';

type Margin = 'sm' | 'md' | 'lg';

@Component({
  selector: 'ava-divider',
  template: `
    <div 
      style="border-top: 2px dashed black; margin: 0 0"
      [style.margin-top.px]="getMargin()"
      [style.margin-bottom.px]="getMargin()"
      [class]="color"
    ></div>
  `,
  styles: [`
    .my-primary {
      border-color: blue !important;
    }
    .my-secondary {
      border-color: red !important;
    }
  `]
})
export class DividerComponent {
  @Input() margin: Margin = 'sm'
  @Input() color: 'my-primary' | 'my-secondary' = 'my-primary'

  getMargin(): number {
    switch(this.margin) {
      case 'sm': return 0;
      case 'md': return 10;
      case 'lg': return 20;
    }
  }
}
