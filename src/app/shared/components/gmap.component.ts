import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ava-gmap',
  template: `
    <img
      width="100%"
      *ngIf="value"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + value + '&size=600,400&zoom=5'" alt="">
  `,
  styles: [
  ]
})
export class GmapComponent  {
  @Input() value: string | null | undefined  = null;

}
