import { Component, Input } from '@angular/core';
import { Gender } from '../../model/user';

@Component({
  selector: 'ava-gender-icon',
  template: `
    <i
      class="fa"
      [style.color]="gender === 'F' ? 'pink' : 'blue'"
      [ngClass]="{
          'fa-mars': gender === 'M',
          'fa-venus': gender === 'F'
        }"
    ></i>
  `,
  styles: [
  ]
})
export class GenderIconComponent {
  @Input() gender: Gender | null = null;
}
