import { Component, Input, OnInit } from '@angular/core';
import { Gender } from '../../model/user';

@Component({
  selector: 'ava-user-label',
  template: `
    <div 
      [ngClass]="{
        'text-info': gender === 'M',
        'text-success': gender === 'F'
      }"
    >
      <i class="fa fa-user"></i>
      {{name}}
    </div>
  `,
})
export class UserLabelComponent {
  @Input() name: string = '';
  @Input() gender: Gender | null = null;
}
