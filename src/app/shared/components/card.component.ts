import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ava-card',
  template: `

    <div class="card">
      <div 
        class="card-header bg-dark text-white" *ngIf="title"
        (click)="opened = !opened"
      >
        {{title}}
        <div class="pull-right" *ngIf="icon">
          <i [class]="icon" (click)="iconClick.emit()"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
      <div class="card-footer">
        <ng-content select=".footer"></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string | null = null;
  @Input() icon: string | null = null;
  @Output() iconClick = new EventEmitter();
  opened = true;
}
