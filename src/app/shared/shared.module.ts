import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenderIconComponent } from './components/gender-icon.component';
import { UserLabelComponent } from './components/user-label.component';
import { CardComponent } from './components/card.component';
import { DividerComponent } from './components/divider.component';
import { TabBarComponent } from './components/tab-bar.component';
import { GmapComponent } from './components/gmap.component';
import { CounterService } from './services/counter.service';



@NgModule({
  declarations: [
    GenderIconComponent,
    UserLabelComponent,
    CardComponent,
    DividerComponent,
    TabBarComponent,
    GmapComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    GenderIconComponent,
    UserLabelComponent,
    CardComponent,
    DividerComponent,
    TabBarComponent,
    GmapComponent,
  ],
  providers: [

  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        CounterService
      ]
    }
  }
}
