import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ava-root',
  template: `
    <ava-navbar></ava-navbar>
    <div style="margin: 0 auto; max-width: 600px; padding: 20px">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
}
