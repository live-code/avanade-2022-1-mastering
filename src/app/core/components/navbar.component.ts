import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ava-navbar',
  template: `
    <button 
      routerLink="login" 
      routerLinkActive="active"
      *ngIf="!authService.isLogged"
    >login</button>
    <button 
      *ngIf="authService.isLogged"
      routerLink="catalog" 
      routerLinkActive="active">catalog</button>
    <button 
      routerLink="home" 
      routerLinkActive="active"
      *ngIf="authService.role === 'admin'"
    >home</button>
    <button routerLink="uikit1" routerLinkActive="active">uikit1</button>
    <button routerLink="uikit2" routerLinkActive="active">uikit2</button>
    <button
      *ngIf="authService.isLogged"
      (click)="logoutHandler()">quit</button>
    
    <div *ngIf="authService.pending">..loading....</div>
    <hr>
    
  `,
  styles: [`
    .active {
      background-color: orange;
    }
  `]
})
export class NavbarComponent  {
  constructor(public authService: AuthService, private router: Router) {
  }

  logoutHandler() {
    this.authService.logout()
    this.router.navigateByUrl('login')
  }
}
