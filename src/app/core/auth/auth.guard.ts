import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { map, Observable, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private http: HttpClient
  ) {
  }

  canActivate(): boolean {
    const isLogged = this.authService.isLogged;
    if (!isLogged) {
      this.router.navigateByUrl('login')
    }
    return isLogged;
  }


  canActivateAsync(): Observable<boolean> {
    const tk = this.authService.token
    return this.http.get<{ response: string }>('http://localhost:3000/validateToken?token='+tk)
      .pipe(
        map(res => res.response === 'ok'),
        tap(isValid => {
          if (!isValid) {
            this.router.navigateByUrl('login')
          }
        })
      )
  }

}
