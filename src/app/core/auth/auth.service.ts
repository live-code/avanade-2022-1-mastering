import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from '../../model/auth';
import { Observable, share, shareReplay, Subscription } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class AuthService {
  auth: Auth | null = null;
  pending = false;

  constructor(private http: HttpClient) {
    const auth: string | null = localStorage.getItem('auth');
    if (auth) {
      this.auth = JSON.parse(auth)
    }
  }

  login(username: string, password: string): Promise<Auth> {
    return new Promise((resolve, reject) => {
      const params = new HttpParams()
        .set('user', username)
        .set('pass', password)

      this.http.get<Auth>('http://localhost:3000/login', { params })
        .subscribe(res => {
          this.auth = res;
          localStorage.setItem('auth', JSON.stringify(res))
          resolve(res)
        })
    })
  }

  logout() {
    this.auth = null;
    localStorage.removeItem('auth')
  }

  get isLogged(): boolean {
   return !!this.auth
  }

  get displayName(): string | null {
    return this.auth ? this.auth.displayName : null
  }

  get role(): string | undefined {
    return this.auth?.role
  }

  get token(): string | undefined {
    return this.auth?.token
  }
}
