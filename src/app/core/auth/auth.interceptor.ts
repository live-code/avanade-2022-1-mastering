import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { catchError, delay, map, Observable, of, tap, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private router: Router,
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.authService.pending = true;
    let cloneReq = req;
    if (this.authService.token && req.url.includes('http://localhost')) {
      cloneReq = req.clone({
        setHeaders: {
          Authorization: this.authService.token,
        }
      })
    }
    return next.handle(cloneReq)
      .pipe(
        map((response) => {
          if (response instanceof HttpResponse) {
            const res = response.clone({
              body: response.body.datum.data
            })
            return res;
          }
          return response;
        }),
        tap(data => console.log(data)),
        delay(environment.production ? 0 : 1000),
        tap(() => this.authService.pending = false),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                this.snackBar.open('404 error!!! ');
                break;

              default:
              case 401:
                this.router.navigateByUrl('login')
                break;
            }
          }
          return throwError(err)
        })
      )
  }

}
