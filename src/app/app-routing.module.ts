import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from './core/auth/auth.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: 'uikit1', loadChildren: () => import('./features/uikit-demo1/uikit-demo1.module').then(m => m.UikitDemo1Module)  },
      { path: 'uikit2', loadChildren: () => import('./features/uikit-demo2/uikit-demo2.module').then(m => m.UikitDemo2Module)  },
      { path: 'uikit2/:countryId', loadChildren: () => import('./features/uikit-demo2/uikit-demo2.module').then(m => m.UikitDemo2Module)  },
      { path: 'catalog', canActivate: [AuthGuard], loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
      { path: '', redirectTo: 'home', pathMatch: 'full'},
    ])
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
