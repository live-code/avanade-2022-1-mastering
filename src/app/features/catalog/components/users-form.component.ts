import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../../model/user';

@Component({
  selector: 'ava-users-form',
  template: `
    <form #f="ngForm" (submit)="addUser.emit(f.value)">
      <input type="text" ngModel name="name" required class="form-control">
      <input type="text" ngModel name="phone" class="form-control">
      <button type="submit" [disabled]="f.invalid">ADD</button>
    </form>
  `,
  styles: [
  ]
})
export class UsersFormComponent {
  @Output() addUser = new EventEmitter<User>();
}
