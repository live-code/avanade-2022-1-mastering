import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ava-users-list',
  template: `
    <div class="bg">Catalog Form</div>
    <li *ngFor="let user of users" class="list-group-item d-flex">
      <ava-gender-icon
        [gender]="user.gender"></ava-gender-icon>

      <ava-user-label
        [name]="user.name"
        [gender]="user.gender"
      ></ava-user-label>

      <i class="fa fa-trash" (click)="deleteUser.emit(user)"></i>
    </li>
  `,
  styles: [`
    .bg { background-color: green}
  `]
})
export class UsersListComponent  {
  @Input() users: User[] = [];
  @Output() deleteUser = new EventEmitter<User>()
}
