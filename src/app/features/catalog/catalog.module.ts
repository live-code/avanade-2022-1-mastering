import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogComponent } from './catalog.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersFormComponent } from './components/users-form.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CatalogService } from './services/catalog.service';



@NgModule({
  declarations: [
    // features: catalog
    CatalogComponent,
    UsersListComponent,
    UsersFormComponent,
  ],
  imports: [
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent}
    ]),
    CommonModule
  ],
  providers: [
    CatalogService
  ],
})
export class CatalogModule { }
