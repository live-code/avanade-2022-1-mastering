import { Injectable } from '@angular/core';
import { User } from '../../../model/user';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, filter, interval, map, of, switchMap, tap } from 'rxjs';

@Injectable()
export class CatalogService {
  users: User[] = [];
  error = false;

  constructor(private http: HttpClient, private router: Router) {

  }

  // -----------1--2----3>
  // filter(x => x > 2)
  // -------------------3>

  getUsers() {
    // 123------->

    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe({
        next: users => {
          console.log(users)
          this.users = users;
        },
        error: () => {
          console.log('ahia!')
          this.error = true;
        }
      })
  }

  addUser(formData: User) {
    console.log(formData)
    this.users.push(formData)
  }

  deleteUser(userToDelete: User) {
    const index = this.users.findIndex(u => u.id === userToDelete.id)
    this.users.splice(index, 1);
  }

  openUrl(url: string) {
    window.open(url)
  }

  destroy() {
    this.users = [];
  }
}
