import { Component, ViewEncapsulation } from '@angular/core';
import { CatalogService } from './services/catalog.service';
import { CounterService } from '../../shared/services/counter.service';

@Component({
  selector: 'ava-catalog',
  template: `
    <h1 class="bg">Catalog</h1>
    
    <div class="alert alert-danger" *ngIf="catalogService.error">AHIA!!</div>
    <ava-users-form 
      (addUser)="catalogService.addUser($event)"></ava-users-form>
    
    <ava-users-list
      [users]="catalogService.users"
      (deleteUser)="catalogService.deleteUser($event)"
    ></ava-users-list>

    <ava-card
      *ngFor="let user of catalogService.users"
      [title]="user.name"
      icon="fa fa-link"
      (iconClick)="catalogService.openUrl('http://' + user.website)"
    ></ava-card>
  `,
})
export class CatalogComponent {
 constructor(public catalogService: CatalogService, public counterSrv: CounterService) {
   catalogService.getUsers()
 }

 ngOnDestroy() {
   console.log('destroy')
   this.catalogService.destroy();
 }
}


