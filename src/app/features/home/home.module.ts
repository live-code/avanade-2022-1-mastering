import { NgModule } from '@angular/core';
import { HomeNewsComponent } from './components/home-news.component';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { HomeGuideComponent } from './home-guide.component';
import { FormsModule } from '@angular/forms';
import { GenderIconComponent } from '../../shared/components/gender-icon.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    HomeComponent,
    HomeNewsComponent,
    HomeGuideComponent,
  ],
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: HomeComponent },
      { path: 'help', component: HomeGuideComponent },
    ])
  ]
})
export class HomeModule {

}

