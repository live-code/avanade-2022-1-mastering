import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { UikitDemo2Component } from './uikit-demo2.component';



@NgModule({
  declarations: [
    UikitDemo2Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: UikitDemo2Component}
    ]),
  ]
})
export class UikitDemo2Module { }
