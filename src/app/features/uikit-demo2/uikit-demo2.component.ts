import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

interface City {
  id: number,
  name: string;
  desc: string;
}
interface Country {
  id: number;
  label: string;
  desc: string;
  cities: City[];
}

@Component({
  selector: 'ava-uikit-demo2',
  template: `
    <h1>Uikit 2</h1>
    <ava-tab-bar 
      [items]="countries"
      [active]="selectedCountry"
      (tabClick)="selectCountryHandler($event)"
    ></ava-tab-bar>

    <ava-tab-bar
      *ngIf="selectedCountry"
      labelField="name"
      [items]="selectedCountry.cities"
      (tabClick)="selectCityHandler($event)"
    ></ava-tab-bar>
    
    <ava-gmap [value]="selectedCity?.name"></ava-gmap>
   
  `,
})
export class UikitDemo2Component {
  selectedCountry: Country | null = null;
  selectedCity: City | null = null;
  countries: Country[] = [];

  constructor(private activatedRoute: ActivatedRoute) {
    const countryId = activatedRoute.snapshot.params['countryId'];
    setTimeout(() => {
      this.countries  = [
        {
          id: 11,
          label: 'Italy',
          desc: 'bla bla 1',
          cities: [
            { id: 1, name: 'Milan', desc: '... 1 ...'},
            { id: 2, name: 'Rome', desc: '... 2 ...'},
            { id: 3, name: 'Palermo', desc: '... 3 ...'},
          ]
        },
        {
          id: 22,
          label: 'Germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, name: 'Berlin', desc: '..wefew.'},
            { id: 2, name: 'Monaco', desc: '..ewfwefwe.'}
          ]
        },
        {
          id: 33,
          label: 'Spain',
          desc: 'bla bla 3',
          cities: [
            { id: 1, name: 'Madrid', desc: '... madrid'}
          ]
        },
      ]
      const index = this.countries.findIndex(c => c.id === +countryId)
      if (index !== -1) {
        this.selectedCountry = this.countries[index]
      }
    }, 1000)
  }

  selectCountryHandler(c: Country) {
    this.selectedCountry = c;
  }

  selectCityHandler(city: City) {
    this.selectedCity = city;
  }
}
