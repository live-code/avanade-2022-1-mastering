import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'ava-uikit-demo1',
  template: `
    <ava-card 
      title="one"
      icon="fa fa-linkedin"
      (iconClick)="openUrl('http://www.linkedin.com')"
    >
      
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt eaque et nam optio quam quidem vel? Accusamus, amet deleniti deserunt expedita maiores neque nihil provident qui, rerum sit, tenetur ullam!
      
      <div class="footer">
        <button (click)="openUrl('http://www.google.com')">
          {{page}}
        </button>
      </div>
    </ava-card>

    <ava-divider margin="md"></ava-divider>
    
    <ava-card 
      title="profile"
      icon="fa fa-plus"
      (iconClick)="visible = true"
    >
      <form>
        <input />
        <input />
        <input />
      </form>
      <div class="footer">
        copyright bla bla 
      </div>
    </ava-card>

    <ava-divider margin="lg" color="my-secondary"></ava-divider>
    
    <ava-card title="multi card">
      <div class="row">
        <div class="col">
          <ava-card title="1">bla bla</ava-card>
        </div>
        <div class="col">
          <ava-card title="2">bla bla</ava-card>
        </div>
      </div>
    
    </ava-card>
    {{page}}
  `,
})
export class UikitDemo1Component {
  page = 'wfweew';
  visible = false;

  constructor(private router: Router) {
  }

  openUrl(url: string) {
    window.open(url)
  }
  changeRounte(route: string) {
    this.router.navigateByUrl(route)
  }
}
