import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { UikitDemo1Component } from './uikit-demo1.component';



@NgModule({
  declarations: [
    UikitDemo1Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: UikitDemo1Component}
    ]),
  ]
})
export class UikitDemo1Module { }
