import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ava-signin',
  template: `
    <h1>Signin </h1>
    
    <form [formGroup]="form" (submit)="loginHandler()">
      <input type="text" formControlName="user">
      <input type="text" formControlName="pass">
      <button type="submit" [disabled]="form.invalid"> LOGIn</button>
    </form>
  `,
})
export class SigninComponent {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    private router: Router
  ) {
    this.form = fb.group({
      user: ['', Validators.required],
      pass: ['', Validators.required],
    })
  }

  loginHandler() {
    const { user, pass } = this.form.value; // destructuring
    this.authService.login(user, pass)
      .then((res) => {
        this.router.navigateByUrl('home')
      })
      .catch(err => {
        window.alert(err)
      })

  }
}
