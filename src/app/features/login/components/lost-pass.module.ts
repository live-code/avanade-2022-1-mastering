import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LostPassComponent } from './lost-pass.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    LostPassComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: LostPassComponent}
    ])
  ]
})
export class LostPassModule { }
