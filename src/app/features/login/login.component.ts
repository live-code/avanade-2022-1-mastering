import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../catalog/services/catalog.service';

@Component({
  selector: 'ava-login',
  template: `
   
    <router-outlet></router-outlet>
    
    <hr>
    
    <button routerLink="signin">signin</button>
    <button routerLink="registration">registration</button>
    <button routerLink="/login/lostpass">Lostpass</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  ngOnInit(): void {
  }

}
