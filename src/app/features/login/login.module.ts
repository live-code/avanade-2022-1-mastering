import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { LostPassComponent } from './components/lost-pass.component';
import { RegistrationComponent } from './components/registration.component';
import { SigninComponent } from './components/signin.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LoginComponent,
    RegistrationComponent,
    SigninComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SigninComponent },
          { path: 'lostpass', loadChildren: () => import('./components/lost-pass.module').then(m => m.LostPassModule) },
          { path: 'registration', component: RegistrationComponent },
          { path: '', redirectTo: 'signin' },
        ]
      },

    ]),
    ReactiveFormsModule
  ]
})
export class LoginModule { }
